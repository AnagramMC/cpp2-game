﻿using UnityEngine;
using System.Collections;

public class bulletScript : MonoBehaviour 
{
	public float bulletSpeed;
    public GameObject explosion;
	private float life;

	// Use this for initialization
	void Start () 
	{
		Destroy (this.gameObject, 2);
	}
	
	// Update is called once per frame
	void Update () 
	{
        GetComponent<Rigidbody>().AddForce (transform.forward * bulletSpeed);
	}

        

	void OnTriggerEnter (Collider c)
	{
		if (c.gameObject.tag == "Obs")
        {
            ObsScript obsScript = c.gameObject.GetComponent<ObsScript>();
            obsScript.health -= 1;
            Instantiate(explosion, this.transform.position, this.transform.rotation);
            Destroy(this.gameObject);
        }
	}
}