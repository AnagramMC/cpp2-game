﻿using UnityEngine;
using System.Collections;

public class spawnEnemy : MonoBehaviour {

    public Transform[] enemySpawn;
    public GameObject[] enemy;

    private int lowerLimit = 0;

	// Use this for initialization
	void Start () {
        enemySpawn[0] = transform.FindChild("SP1");
        enemySpawn[1] = transform.FindChild("SP2");
        enemySpawn[2] = transform.FindChild("SP3");

        SpawnEnemy(Random.Range(0, 3));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void SpawnEnemy(int x)
    {
        int y = Random.Range(lowerLimit, 10);
        switch (y)
        {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
                if (enemy[0] != null)
                    Instantiate(enemy[0], enemySpawn[x].transform.position, enemySpawn[x].transform.rotation);
                break;
            case 8:
            case 9:
                if (enemy[1] != null)
                    Instantiate(enemy[1], enemySpawn[x].transform.position, enemySpawn[x].transform.rotation);
                break;
        }

    }
}
