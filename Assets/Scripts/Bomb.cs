﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {

    private SphereCollider triggerRadius;
	// Use this for initialization
	void Start () {
        Destroy(this.gameObject, 3);
        triggerRadius = GetComponent<SphereCollider>();
	}
	
	// Update is called once per frame
	void Update () {
        triggerRadius.radius += 0.05f; 
	}
}
