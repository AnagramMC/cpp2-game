﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    private int port = 25000;
    public GameObject player;
    public GameObject p1;
    public GameObject p2;
    private string IP = "127.0.0.1";
    private int maxClient = 1;
    NetworkView networkView;
    HostData[] hosts = null;
    public Canvas buttons;


    void OnGUI()
    {
        if (hosts != null && hosts.Length > 0)
        {
            if (GUI.Button(new Rect(25, 105, 200, 30), hosts[0].gameName))
            {
                print(Network.Connect(hosts[0]).ToString());
                print(hosts[0]);
            }
        }
    }


    void StartGame()
    {
        Application.LoadLevel(1);
    }

    void QuitGame() 
    {
        Application.Quit();
    }

    void MultiGame()
    {
        Application.LoadLevel(2);
    }


    void StartServer()
    {
        Network.InitializeServer(2, port, false);
        MasterServer.RegisterHost("Runner", "NetworkTest");
    }

    void stopServer()
    {
        Network.Disconnect();
    }

    void connectToServer()
    {
        StartCoroutine("GetServerList");
    }

    public IEnumerator GetServerList()
    {
        MasterServer.RequestHostList("Runner");
        float timeStarted = Time.time;
        float timeEnd = Time.time + 3;

        while (Time.time < timeEnd)
        {
            hosts = MasterServer.PollHostList();
            yield return new WaitForEndOfFrame();
        }
    }
    
    void OnConnectedToServer()
    {
        Debug.Log("Connected");
        buttons.gameObject.SetActive(false);
        Network.Instantiate(player, p2.transform.position, p2.transform.rotation, 0);

    }

    void OnPlayerConnected()
    {
        Debug.Log("P Connected");
        buttons.gameObject.SetActive(false);
        Network.Instantiate(player, p1.transform.position, p1.transform.rotation, 0);
    }

    void FailedToConnect(NetworkConnectionError error)
    {
        print("FailedToConnect--" + error.ToString());
    }
}
