﻿using UnityEngine;
using System.Collections;

public class Scrolling : MonoBehaviour {

    public float speed;
    public GameObject spawner3;

	// Use this for initialization
	void Start () {
        spawner3 = GameObject.FindGameObjectWithTag("Respawn");
	}
	
	// Update is called once per frame
	void Update () {
        transform.position -= new Vector3(0, 0, 0.08f);

        if (transform.position.z <= -12)
        {
            int x = Random.Range(0, 3);
            spawner3.SendMessage("SpawnObject", x);
            Destroy(this.gameObject);
        }
	}
}
