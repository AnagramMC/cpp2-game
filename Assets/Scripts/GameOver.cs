﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOver : MonoBehaviour {

    public int distance;
    public Text distanceTotal;
    
	// Use this for initialization
	void Start () {
        distanceTotal.text = "Distance Ran: " + distance + " meters!";
        
	}
	
    void Quit()
    {
        Application.Quit();
    }

    void Retry()
    {
        Application.LoadLevel(1);
    }
}
