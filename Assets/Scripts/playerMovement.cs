﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class playerMovement : MonoBehaviour {

    public GameObject weapon;
    public GameObject bullet;
    public GameObject bombPrefab;
    public GameObject shield;
    public Image bombCounter;
    public Text distanceCounter;
    public Canvas dead;
    public bool networkGame;

    public float speed;
    public int ammo = 0;
    private float time;
    public float jumpSpeed;
    //public float gravity;

    private bool isJumping;
    private bool isMoving;
    private bool cooldown;
    private int distance;

    private Animation anim;
    private Animator UIAnim;
    //private CharacterController controller;
    Vector3 currentMovement;
    
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animation>();
        UIAnim = bombCounter.gameObject.GetComponent<Animator>();
        Distance();
        if (Network.isServer || Network.isClient)
        {
            networkGame = true;
        }

        //controller = this.gameObject.GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.N))
        {
            if (!cooldown)
            {
                Instantiate(bullet, weapon.transform.position, Quaternion.identity);
                SetCooldown();
            }
        }

        if (Input.GetKeyDown(KeyCode.B))
        {
            if (ammo > 0)
            {
                Instantiate(bombPrefab, this.transform.position, Quaternion.identity);
                ammo -= 1;
                Debug.Log(ammo);
            }
        }

        if (time > 0)
        {
            time -= Time.deltaTime;
        }
        else if (time <= 0)
        {
            Deactivate();
        }

        UIAnim.SetInteger("ammo", ammo);
    }

    void Distance()
    {
        distance++;
        distanceCounter.text = "Distance Ran: " + distance + " meters";
        Invoke("Distance", 1);
    }

    void SetCooldown()
    {
        cooldown = true;
        Invoke("CooldownReset", 1);
    }

    void CooldownReset()
    {
        cooldown = false;
    }

	void FixedUpdate () {
        float hAxis = Input.GetAxisRaw("Horizontal") * speed * Time.deltaTime;
        float vAxis = Input.GetAxisRaw("Vertical") * speed * Time.deltaTime;

        //currentMovement = new Vector3(hAxis, rigidbody.velocity.y, vAxis);

        //rigidbody.velocity = currentMovement;


        if (Input.GetAxisRaw("Horizontal") == -1)
        {
            GetComponent<Rigidbody>().velocity = new Vector3(hAxis, GetComponent<Rigidbody>().velocity.y, GetComponent<Rigidbody>().velocity.z);
            isMoving = true;
            anim.Play("strafeLeft");
        }

        if (Input.GetAxisRaw("Horizontal") == 1)
        {
            GetComponent<Rigidbody>().velocity = new Vector3(hAxis, GetComponent<Rigidbody>().velocity.y, GetComponent<Rigidbody>().velocity.z);
            isMoving = true;
            anim.Play("strafeRight");
        }


        if (!isJumping)
        {
            if (Input.GetKey(KeyCode.Space))
            {
                GetComponent<Rigidbody>().AddForce(new Vector3(0, jumpSpeed, 0), ForceMode.Force);
                isJumping = true;
                anim.Play("jump");
            }
        }

        //if (rigidbody.velocity == Vector3.zero)
        //{
        //    isMoving = false;
        //}

        //if (!controller.isGrounded)
        //{
        //    currentMovement -= new Vector3(0, gravity * Time.deltaTime, 0);
        //}
        //else
        //{
        //    currentMovement.y = 0;
        //    if (Input.GetKey(KeyCode.Space))
        //    {
        //        currentMovement.y = jumpSpeed;
        //    }
        //}

        //currentMovement.x -= 0.000000000001f * Time.deltaTime;

        //controller.Move(currentMovement);

        
	}

    void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.tag == "Wall")
        {
            Dead();
        }
    }

    void Dead()
    {
        dead.gameObject.SetActive(true);
        GameOver g = dead.gameObject.GetComponent<GameOver>();
        g.distance = distance;
        Destroy(this.gameObject);
    }

    void OnCollisionStay(Collision c)
    {
        if (c.gameObject.tag == "Ground")
        {
            isJumping = false;
            anim.Play("sprint");
        }
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "shield")
        {
            Destroy(c.gameObject);
            shield.SetActive(true);
            time = 30;
        }
        if (c.gameObject.tag == "bombPick")
        {
            if (ammo < 3)
            {
                Destroy(c.gameObject);
                ammo += 1;
            }
        }
    }

    void Deactivate()
    {
        shield.SetActive(false);
    }
}
