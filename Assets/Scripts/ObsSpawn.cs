﻿using UnityEngine;
using System.Collections;

public class ObsSpawn : MonoBehaviour {

    public GameObject[] obsSpawn;
    
    private int spawnCount = 0;


	// Use this for initialization
	void Start () {
        SpawnObject(Random.Range(0, 3));
	}
	
    void Shuffle()
    {
        GameObject temp = obsSpawn[0];
        int x = 0;
            while (x < obsSpawn.Length)
            {
                if (x + 1 == obsSpawn.Length)
                {
                    obsSpawn[x] = temp;
                    break;
                }
                else
                {
                    obsSpawn[x] = obsSpawn[x + 1];
                    x++;
                }
            }
    }

    void SpawnObject(int x)
    {
        Instantiate(obsSpawn[x], transform.position, Quaternion.identity);
        spawnCount++;
        if (spawnCount <= 6)
        {
            Shuffle();
        }

    }

    

}
