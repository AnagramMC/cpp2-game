﻿using UnityEngine;
using System.Collections;

public class ObsScript : MonoBehaviour {

    public int health = 3;
    
    public GameObject[] pickups;

    private bool maxY = false;
    private bool addedForce = false;

	// Use this for initialization
	void Start () {
        //int i = 0;
        //while (i < pickups.Length)
        //{
        //    pickups[i] = GameObject.Find("Pickup" + i);
        //    i++;
        //}
	}

    void CheckY()
    {
        //if (!addedForce)
        //{
        //    rigidbody.AddForce(new Vector3(0, 0, -200), ForceMode.Acceleration);
        //    addedForce = true;
        //}
    }
	
	// Update is called once per frame
	void Update () {
        if (!maxY)
            transform.Translate(new Vector3(0, 0.01f, 0));

        if (transform.position.y >= -0.5f)
        {
            maxY = true;
            CheckY();
        }

        //if (transform.position.z <= -4.5f)
        //{
        //    Destroy(this.gameObject);
        //}

        if (health <= 0)
        {
            int pickupChance = Random.Range(0, 50);
            switch (pickupChance)
            {
                case 0:
                    //GameObject pickup0 = Instantiate(pickups[0], transform.position, transform.rotation) as GameObject;
                    //pickup0.transform.parent = this.transform.parent;
                    //break;
                case 1:
                    //GameObject pickup1 = Instantiate(pickups[1], transform.position, transform.rotation) as GameObject;
                    //pickup1.transform.parent = this.transform.parent;
                    //break;
                case 2:
                    //GameObject pickup2 = Instantiate(pickups[2], transform.position, transform.rotation) as GameObject;
                    //pickup2.transform.parent = this.transform.parent;
                    //break;
                case 3:
                    //GameObject pickup3 = Instantiate(pickups[3], transform.position, transform.rotation) as GameObject;
                    //pickup3.transform.parent = this.transform.parent;
                    //break;
                case 4:
                    GameObject pickup4 = Instantiate(pickups[4], transform.position, transform.rotation) as GameObject;
                    pickup4.transform.parent = this.transform.parent;
                    break;
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    GameObject pickup5 = Instantiate(pickups[5], transform.position, transform.rotation) as GameObject;
                    pickup5.transform.parent = this.transform.parent;
                    break;
            }
            Destroy(this.gameObject);
        }
	}

    void OnCollisionEnter (Collision c)
    {
        if (c.gameObject.tag == "Wall")
        {
            this.GetComponent<Collider>().isTrigger = true;
            //rigidbody.AddForce(new Vector3(0, 0, -200), ForceMode.Acceleration);
        }

        //if (c.gameObject.tag == "Player")
        //{
        //    rigidbody.AddForce(new Vector3(0, 0, -c.gameObject.rigidbody.velocity.z));
        //}
    }

    void OnTriggerEnter(Collider c)
    {
        if (c.gameObject.tag == "shield")
        {
            Destroy(this.gameObject);
        }

        if (c.gameObject.tag == "bomb")
        {
            Destroy(this.gameObject);
        }
    }

    void OnTriggerExit (Collider c)
    {
        if (c.gameObject.tag == "NWall")
        {
            this.GetComponent<Collider>().isTrigger = false;
        }
    }

}
