# README #

A and D to move.
Space to jump.
N to shoot.
B for bombs.

Pickups include shield and bombs.

Issues encountered:
Original network solution was to load a separate level for the client and server but after a few days of issues, I moved over to a different solution.  Now while the level does load, the build version seems slower then the editor version and cameras are overlapping.  Players control their own player, but the camera does not seem to be working as a separate entity.